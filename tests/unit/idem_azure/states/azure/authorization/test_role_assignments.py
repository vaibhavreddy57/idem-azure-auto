import copy
from collections import ChainMap

import pytest


RESOURCE_NAME = "my-role-assignment"
ROLE_ASSIGNMENT_NAME = "my-role-assignment"
ROLE_DEFINITION_ID = "b24988ac-6180-42a0-ab88-20f7382dd24c"


@pytest.mark.asyncio
async def test_present_resource_not_exists(hub, mock_hub, ctx):
    """
    Test 'present' state of role assignments. When a resource does not exist, 'present' should create the resource.
    """
    mock_hub.states.azure.authorization.role_assignments.present = (
        hub.states.azure.authorization.role_assignments.present
    )
    mock_hub.exec.azure.authorization.role_assignments.get = (
        hub.exec.azure.authorization.role_assignments.get
    )
    mock_hub.tool.azure.authorization.role_assignments.convert_raw_role_assignments_to_present = (
        hub.tool.azure.authorization.role_assignments.convert_raw_role_assignments_to_present
    )
    mock_hub.tool.azure.authorization.role_assignments.convert_present_to_raw_role_assignments = (
        hub.tool.azure.authorization.role_assignments.convert_present_to_raw_role_assignments
    )
    mock_hub.tool.azure.test_state_utils.generate_test_state = (
        hub.tool.azure.test_state_utils.generate_test_state
    )
    scope = f"subscriptions/{ctx.acct.subscription_id}"
    resource_parameters_raw = {
        "properties": {
            "roleDefinitionId": f"{scope}/providers/Microsoft.Authorization/roleDefinitions/{ROLE_DEFINITION_ID}",
            "principalId": "d93a38bc-d029-4160-bfb0-fbda779ac214",
            "scope": scope,
        },
    }
    resource_parameters = {
        "scope": scope,
        "role_definition_id": f"{scope}/providers/Microsoft.Authorization/roleDefinitions/{ROLE_DEFINITION_ID}",
        "principal_id": "d93a38bc-d029-4160-bfb0-fbda779ac214",
        "role_assignment_name": ROLE_ASSIGNMENT_NAME,
    }

    expected_get = {
        "ret": {},
        "result": False,
        "status": 404,
        "comment": "Not Found.",
    }
    expected_put = {
        "ret": {
            "id": f"{scope}/providers/Microsoft.Authorization/roleAssignments/{ROLE_ASSIGNMENT_NAME}",
            "name": RESOURCE_NAME,
            **resource_parameters_raw,
        },
        "result": True,
        "status": 200,
        "comment": "",
    }

    def _check_get_parameters(_ctx, url, success_codes):
        assert scope in url
        assert ROLE_ASSIGNMENT_NAME in url
        return expected_get

    def _check_put_parameters(_ctx, url, success_codes, json):
        assert scope in url
        assert ROLE_ASSIGNMENT_NAME in url
        assert (
            resource_parameters_raw["properties"]["roleDefinitionId"]
            == json["properties"]["roleDefinitionId"]
        )
        assert (
            resource_parameters_raw["properties"]["principalId"]
            == json["properties"]["principalId"]
        )
        return expected_put

    mock_hub.exec.request.json.get.side_effect = _check_get_parameters

    # Test present() with --test flag on
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    ret = await mock_hub.states.azure.authorization.role_assignments.present(
        test_ctx,
        RESOURCE_NAME,
        **resource_parameters,
    )
    assert ret["result"], ret["comment"]
    assert not ret["old_state"]
    assert ret["new_state"]
    assert RESOURCE_NAME == ret["new_state"].get("name")
    assert (
        f"Would create azure.authorization.role_assignments '{RESOURCE_NAME}'"
        in ret["comment"]
    )
    check_returned_states(
        old_state=None,
        new_state=ret["new_state"],
        expected_old_state=None,
        expected_new_state=resource_parameters,
    )

    # Turn on put.side_effect after testing --test, since PUT operation should not be called in --test
    mock_hub.exec.request.json.put.side_effect = _check_put_parameters

    # Test present() with --test flag off
    ret = await mock_hub.states.azure.authorization.role_assignments.present(
        ctx,
        RESOURCE_NAME,
        **resource_parameters,
    )
    assert ret["result"], ret["comment"]
    assert not ret["old_state"]
    assert ret["new_state"]
    assert RESOURCE_NAME == ret["new_state"].get("name")
    assert (
        f"Created azure.authorization.role_assignments '{RESOURCE_NAME}'"
        in ret["comment"]
    )
    check_returned_states(
        old_state=None,
        new_state=ret["new_state"],
        expected_old_state=None,
        expected_new_state=resource_parameters,
    )


@pytest.mark.asyncio
async def test_present_resource_exists(hub, mock_hub, ctx):
    """
    Test 'present' state of role assignments. When a resource exists, 'present' will return the existing resource,
     since Azure role assignments doesn't support update any parameter.
    """
    mock_hub.states.azure.authorization.role_assignments.present = (
        hub.states.azure.authorization.role_assignments.present
    )
    mock_hub.exec.azure.authorization.role_assignments.get = (
        hub.exec.azure.authorization.role_assignments.get
    )
    mock_hub.tool.azure.authorization.role_assignments.convert_raw_role_assignments_to_present = (
        hub.tool.azure.authorization.role_assignments.convert_raw_role_assignments_to_present
    )

    scope = f"subscriptions/{ctx.acct.subscription_id}"
    resource_parameters_raw = {
        "properties": {
            "roleDefinitionId": f"{scope}/providers/Microsoft.Authorization/roleDefinitions/{ROLE_DEFINITION_ID}",
            "principalId": "d93a38bc-d029-4160-bfb0-fbda779ac214",
            "scope": scope,
        },
    }
    resource_parameters = {
        "scope": scope,
        "role_definition_id": f"{scope}/providers/Microsoft.Authorization/roleDefinitions/{ROLE_DEFINITION_ID}",
        "principal_id": "d93a38bc-d029-4160-bfb0-fbda779ac214",
        "role_assignment_name": ROLE_ASSIGNMENT_NAME,
    }

    expected_get = {
        "ret": {
            "id": f"{scope}/providers/Microsoft.Authorization/roleAssignments/{ROLE_ASSIGNMENT_NAME}",
            "name": RESOURCE_NAME,
            **resource_parameters_raw,
        },
        "result": True,
        "status": 200,
        "comment": "",
    }

    mock_hub.exec.request.json.get.return_value = expected_get

    ret = await mock_hub.states.azure.authorization.role_assignments.present(
        ctx, RESOURCE_NAME, **resource_parameters
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"]
    assert ret["new_state"]
    assert RESOURCE_NAME == ret["old_state"].get("name")
    assert RESOURCE_NAME == ret["new_state"].get("name")
    assert (
        f"azure.authorization.role_assignments '{RESOURCE_NAME}' has no property to be updated."
        in ret["comment"]
    )
    check_returned_states(
        old_state=ret["old_state"],
        new_state=ret["new_state"],
        expected_old_state=resource_parameters,
        expected_new_state=resource_parameters,
    )


@pytest.mark.asyncio
async def test_absent_resource_not_exists(hub, mock_hub, ctx):
    """
    Test 'absent' state of role assignments. When a resource does not exist, 'absent' should just return success.
    """
    mock_hub.states.azure.authorization.role_assignments.absent = (
        hub.states.azure.authorization.role_assignments.absent
    )
    mock_hub.exec.azure.authorization.role_assignments.get = (
        hub.exec.azure.authorization.role_assignments.get
    )

    scope = f"subscriptions/{ctx.acct.subscription_id}"

    expected_get = {
        "ret": {},
        "result": False,
        "status": 404,
        "comment": "Not Found.",
    }

    def _check_get_parameters(_ctx, url, success_codes):
        assert scope in url
        assert ROLE_ASSIGNMENT_NAME in url
        return expected_get

    mock_hub.exec.request.json.get.side_effect = _check_get_parameters

    ret = await mock_hub.states.azure.authorization.role_assignments.absent(
        ctx, RESOURCE_NAME, scope, ROLE_ASSIGNMENT_NAME
    )
    assert ret["result"], ret["comment"]
    assert not ret["old_state"]
    assert not ret["new_state"]
    assert (
        f"azure.authorization.role_assignments '{RESOURCE_NAME}' already absent"
        in ret["comment"]
    )


@pytest.mark.asyncio
async def test_absent_resource_exists(hub, mock_hub, ctx):
    """
    Test 'absent' state of role assignments. When a resource exists, 'absent' should delete the resource.
    """
    mock_hub.states.azure.authorization.role_assignments.absent = (
        hub.states.azure.authorization.role_assignments.absent
    )
    mock_hub.exec.azure.authorization.role_assignments.get = (
        hub.exec.azure.authorization.role_assignments.get
    )
    mock_hub.tool.azure.uri.get_parameter_value_in_dict = (
        hub.tool.azure.uri.get_parameter_value_in_dict
    )
    mock_hub.tool.azure.authorization.role_assignments.convert_raw_role_assignments_to_present = (
        hub.tool.azure.authorization.role_assignments.convert_raw_role_assignments_to_present
    )
    mock_hub.tool.azure.test_state_utils.generate_test_state = (
        hub.tool.azure.test_state_utils.generate_test_state
    )

    scope = f"subscriptions/{ctx.acct.subscription_id}"
    resource_parameters_raw = {
        "properties": {
            "roleDefinitionId": f"{scope}/providers/Microsoft.Authorization/roleDefinitions/{ROLE_DEFINITION_ID}",
            "principalId": "d93a38bc-d029-4160-bfb0-fbda779ac214",
            "scope": scope,
        },
    }
    resource_parameters = {
        "scope": scope,
        "role_definition_id": f"{scope}/providers/Microsoft.Authorization/roleDefinitions/{ROLE_DEFINITION_ID}",
        "principal_id": "d93a38bc-d029-4160-bfb0-fbda779ac214",
        "role_assignment_name": ROLE_ASSIGNMENT_NAME,
    }

    expected_get = {
        "ret": {
            "id": f"{scope}/providers/Microsoft.Authorization/roleAssignments/{ROLE_ASSIGNMENT_NAME}",
            "name": RESOURCE_NAME,
            **resource_parameters_raw,
        },
        "result": True,
        "status": 200,
        "comment": "Found.",
    }
    expected_delete = {
        "ret": {},
        "result": True,
        "status": 200,
        "comment": "Deleted",
    }

    def _check_delete_parameters(_ctx, url, success_codes):
        assert scope in url
        assert ROLE_ASSIGNMENT_NAME in url
        return expected_delete

    mock_hub.exec.request.json.get.return_value = expected_get

    # Test absent() with --test flag on
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    ret = await mock_hub.states.azure.authorization.role_assignments.absent(
        test_ctx, RESOURCE_NAME, scope, ROLE_ASSIGNMENT_NAME
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"]
    assert not ret["new_state"]
    assert (
        f"Would delete azure.authorization.role_assignments '{RESOURCE_NAME}'"
        in ret["comment"]
    )
    assert RESOURCE_NAME == ret["old_state"].get("name")
    check_returned_states(
        old_state=ret["old_state"],
        new_state=None,
        expected_old_state=resource_parameters,
        expected_new_state=None,
    )

    mock_hub.exec.request.raw.delete.side_effect = _check_delete_parameters
    # Test absent() with --test flag off
    ret = await mock_hub.states.azure.authorization.role_assignments.absent(
        ctx, RESOURCE_NAME, scope, ROLE_ASSIGNMENT_NAME
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"]
    assert not ret["new_state"]
    assert (
        f"Deleted azure.authorization.role_assignments '{RESOURCE_NAME}'"
        in ret["comment"]
    )
    assert RESOURCE_NAME == ret["old_state"].get("name")
    check_returned_states(
        old_state=ret["old_state"],
        new_state=None,
        expected_old_state=resource_parameters,
        expected_new_state=None,
    )


@pytest.mark.asyncio
async def test_describe(hub, mock_hub, ctx):
    """
    Test 'describe' state of role assignments.
    """
    mock_hub.states.azure.authorization.role_assignments.describe = (
        hub.states.azure.authorization.role_assignments.describe
    )
    mock_hub.tool.azure.request.paginate = hub.tool.azure.request.paginate
    mock_hub.tool.azure.uri.get_parameter_value_in_dict = (
        hub.tool.azure.uri.get_parameter_value_in_dict
    )
    mock_hub.exec.azure.authorization.role_assignments.list = (
        hub.exec.azure.authorization.role_assignments.list
    )
    mock_hub.tool.azure.authorization.role_assignments.convert_raw_role_assignments_to_present = (
        hub.tool.azure.authorization.role_assignments.convert_raw_role_assignments_to_present
    )
    scope = f"subscriptions/{ctx.acct.subscription_id}"
    resource_id = f"/{scope}/providers/Microsoft.Authorization/roleAssignments/{ROLE_ASSIGNMENT_NAME}"
    resource_parameters_raw = {
        "properties": {
            "roleDefinitionId": f"{scope}/providers/Microsoft.Authorization/roleDefinitions/{ROLE_DEFINITION_ID}",
            "principalId": "d93a38bc-d029-4160-bfb0-fbda779ac214",
            "scope": scope,
        },
    }
    resource_parameters = {
        "scope": scope,
        "role_definition_id": f"{scope}/providers/Microsoft.Authorization/roleDefinitions/{ROLE_DEFINITION_ID}",
        "principal_id": "d93a38bc-d029-4160-bfb0-fbda779ac214",
        "role_assignment_name": ROLE_ASSIGNMENT_NAME,
    }
    expected_list = {
        "ret": {
            "value": [
                {"id": resource_id, "name": RESOURCE_NAME, **resource_parameters_raw}
            ]
        },
        "result": True,
        "status": 200,
        "comment": "Found.",
    }

    mock_hub.exec.request.json.get.return_value = expected_list

    ret = await mock_hub.states.azure.authorization.role_assignments.describe(ctx)

    assert resource_id == list(ret.keys())[0]
    ret_value = ret.get(resource_id)
    assert "azure.authorization.role_assignments.present" in ret_value.keys()
    described_resource = ret_value.get("azure.authorization.role_assignments.present")
    described_resource_map = dict(ChainMap(*described_resource))
    assert resource_id == described_resource_map.get("name")
    check_returned_states(
        old_state=None,
        new_state=described_resource_map,
        expected_old_state=None,
        expected_new_state=resource_parameters,
    )


def check_returned_states(old_state, new_state, expected_old_state, expected_new_state):
    if old_state:
        assert ROLE_ASSIGNMENT_NAME == old_state.get("role_assignment_name")
        assert expected_old_state["scope"] == old_state.get("scope")
        assert expected_old_state["role_definition_id"] == old_state.get(
            "role_definition_id"
        )
        assert expected_old_state["principal_id"] == old_state.get("principal_id")
    if new_state:
        assert expected_new_state["scope"] == new_state.get("scope")
        assert expected_new_state["role_definition_id"] == new_state.get(
            "role_definition_id"
        )
        assert expected_new_state["principal_id"] == new_state.get("principal_id")
